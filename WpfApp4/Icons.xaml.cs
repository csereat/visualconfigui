﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace WpfApp4
{

    /// <summary>
    /// Interaction logic for Icons.xaml
    /// </summary>
    public partial class Icons : Window
    {
        Button btnStlButtonActive;
        List<Button> IconsListOfButtons = new List<Button>();
        public Icons(Button btnStlButtonActive)
        {
            InitializeComponent();
            this.btnStlButtonActive = btnStlButtonActive;
            ResourceManager resourceManager = new ResourceManager(typeof(Properties.Resources));
            ResourceSet resourceSet = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            string savedPicturePath = Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config) + @"\Icons\";
            foreach (DictionaryEntry entry in resourceSet)
            {
                object resource = entry.Value;
                Bitmap bitmap = (Bitmap)resource;
                IntPtr hBitmap = bitmap.GetHbitmap();
                BitmapSource hhh = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                
                string savedPicturePathandName = savedPicturePath + entry.Key.ToString().Replace("_", " ") + ".png";
                if (!File.Exists(savedPicturePathandName))
                {
                    bitmap.Save(savedPicturePathandName);
                }
            }
            foreach (string strButton in Directory.GetFiles(savedPicturePath))
            {
                Button imageButton = new Button
                {
                    Content = new System.Windows.Controls.Image()
                    {
                        Source = new BitmapImage(new Uri(strButton))
                    },
                    Name = Path.GetFileNameWithoutExtension(strButton).Replace(" ","_"),
                    Width = 30,
                    Height = 30,
                    Margin = new Thickness(5, 5, 5, 5),
                    Tag = @"\Icons\"+ Path.GetFileName(strButton)
                };
                imageButton.Click += new RoutedEventHandler(imageButton_Click);
                IconsListOfButtons.Add(imageButton);
            }
            foreach (Button bt in IconsListOfButtons)
            {
                wrappanel.Children.Add(bt);
            }
            IconsListOfButtons.Sort(delegate (Button x, Button y)
            {
                if (x.Name == null && y.Name == null) return 0;
                else if (x.Name == null) return -1;
                else if (y.Name == null) return 1;
                else return x.Name.CompareTo(y.Name);
            });

        }
        private void imageButton_Click(object sender, RoutedEventArgs e)
        {
            Button btnSelectedFronList = (Button)sender;
            btnStlButtonActive.Tag = btnSelectedFronList.Tag;
            btnStlButtonActive.Content = new System.Windows.Controls.Image()
            {
                Source = new BitmapImage(new Uri(Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config) + btnSelectedFronList.Tag.ToString(), UriKind.Absolute)),
            };

           
            this.Close();
        }
        
        
           


        private void BrowseImage_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Filter = "Image files(*.png) | *.png";
            openFileDialog.Title = "Please select an image to upload";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                if (Path.GetExtension(openFileDialog.FileName).ToLower() != ".png")
                {
                    MessageBox.Show("The selected file is not PNG" + Environment.NewLine + "Only PNG files are allowed", "Invalid file format", MessageBoxButton.OK);
                }
                else
                {
                    string DestPictFilename = Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config) + @"\Icons\" + openFileDialog.SafeFileName;
                    if (!File.Exists(DestPictFilename))
                    {
                        File.Copy(openFileDialog.FileName, DestPictFilename);
                    }
                    Button imageButton = new Button
                    {
                        Content = new System.Windows.Controls.Image()
                        {
                            Source = new BitmapImage(new Uri(openFileDialog.FileName))
                        },
                        Name = openFileDialog.SafeFileName.Remove(openFileDialog.SafeFileName.Length - 4, 4),
                        Width = 30,
                        Height = 30,
                        Margin = new Thickness(5, 5, 5, 5),
                        Tag = DestPictFilename
                    };
                    imageButton.Click += new RoutedEventHandler(imageButton_Click);
                    wrappanel.Children.Add(imageButton);
                } 
                
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
        private void Container_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
       
    }
}
