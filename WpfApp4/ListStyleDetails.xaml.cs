﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp4
{
    /// <summary>
    /// Interaction logic for ListStyleDetails.xaml
    /// </summary>
    public partial class ListStyleDetails : Window
    {
        public ListStyleDetails(List<string> chkTag)
        {
            InitializeComponent();
            double Xpos = 0;
            double Ypos = 0;
            int i = 0;
            foreach (string listitem in chkTag)
            {
                if (i > 0)
                {
                    Label LLlabel = new Label();
                    LLlabel.Content = listitem;
                    LLlabel.FontSize = 10;
                    LLlabel.Margin = new Thickness(Xpos, Ypos, 0, 0);
                    Xpos = Xpos + 20;
                    Ypos = Ypos + 17;
                    continer.Children.Add(LLlabel);
                }
                i++;
            }
        }
    }
}
