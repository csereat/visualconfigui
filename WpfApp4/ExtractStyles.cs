﻿using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using WpfApp4.Properties;


namespace WpfApp4
{
    
    class ExtractStyles
    {
        
        
        static List<string> ExcludedElements = new List<string> { "rsid", "docId" };  
        static List<string> ExcludedAttributes = new List<string> { "settings", "sectPr"}; 
        dynamic JsonrootElement;
        StyleDicList _StyleDicList;

        public void extractStyles(string filenamewpath,TextBox resultBox, List<string> stlList, List<string> tblList, List<List<string>> Lists, string Config, StyleDicList _StyleDicList)
        {
            this._StyleDicList = _StyleDicList;
            JsonrootElement = new ExpandoObject();
            string path = Path.Combine(Settings.Default.ConfigPath, Config);

            var assembly = Assembly.GetExecutingAssembly();
            string result = "";
            

            List<ExpandoObject> JsonList = new List<ExpandoObject>();

            var resourceName = "WpfApp4.dictionary.ooxml_interop.txt";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
            dynamic O2IDictionary = JObject.Parse(result);

            #region FileDefinition
            resultBox.AppendText("Extracting file settings..." + Environment.NewLine);
            JsonrootElement.FileDefinitionPart = new ExpandoObject();

            #region Document Part - sections
            XDocument ExtractedDocument = ExtractPart(filenamewpath, "document", false);
            XNamespace ns = ExtractedDocument.Root.GetNamespaceOfPrefix("w");
            JsonList = new List<ExpandoObject>();

            foreach (XElement section in ExtractedDocument.Root.Descendants(ns + "sectPr"))
            {
                ProcessElements(section, JsonrootElement.FileDefinitionPart, O2IDictionary.document.sectPr, null, JsonList);
            }

            #endregion

            #region Document Part - TOC and fields
            
            var FDPart = (IDictionary<string, Object>)JsonrootElement.FileDefinitionPart;
           
            IList<string> FieldFormats = new List<string>();

            XElement prevParent = ExtractedDocument.Root;
            string prevNodeValue ="";

            foreach (XElement itext in ExtractedDocument.Root.Descendants(ns + "instrText"))
            {
                if (itext.Value.Length>=8 && itext.Value.Substring(0, 8) != " PAGEREF")
                {
                    if (itext.Parent == prevParent)
                    {
                        FieldFormats[FieldFormats.Count - 1] = FieldFormats.Last().Replace(FieldFormats.Last(), FieldFormats.Last() + "|" + itext.Value);
                    }
                    else
                    {
                        if (!FieldFormats.Contains(itext.Value))
                        {
                            FieldFormats.Add(itext.Value);
                        }
                    }
                    prevParent = itext.Parent;                   
                 }
            }

            prevParent = ExtractedDocument.Root;
            string NodeValue;
            foreach (XElement itext in ExtractedDocument.Root.Descendants(ns + "fldSimple"))
            {

                if (itext.Parent == prevParent)
                {
                    FieldFormats[FieldFormats.Count-1] = FieldFormats.Last().Replace(FieldFormats.Last(), FieldFormats.Last() + "|" + itext.Attribute(ns + "instr").Value);
                }
                else
                {
                    if (!FieldFormats.Contains(itext.Attribute(ns + "instr").Value))
                    { 
                         FieldFormats.Add(itext.Attribute(ns + "instr").Value);
                    }
                }
                
                prevParent = itext.Parent;
            }

            if (FieldFormats.Count != 0)           
            FDPart.Add("FieldFormats", FieldFormats.Distinct());

            List<CaptionConfig> captionConfigList = new List<CaptionConfig>();

            foreach (string field in FieldFormats)
            {
                if (field.Contains("TOC")) // table of contents - tempTOC
                {

                }
                else if (field.Contains("SEQ"))  //captionconfig 
                {
                    CaptionConfig tempcc = new CaptionConfig();

                    if (field.Contains("STYLEREF"))
                    {
                        tempcc.IncludeChapterNumber = true;
                        tempcc.ChapterStyleLevel = Convert.ToInt32(field.Substring(10,1)); 
                    }

                    captionConfigList.Add(tempcc);
                }
            }

            #endregion

            #region Table List

            Dictionary<string, List<TableView>> TableViewList = new Dictionary<string, List<TableView>>();
            int viewInd=0;
            foreach (XElement tablePrNode in ExtractedDocument.Root.Descendants(ns + "tblPr"))
            {
                
                string ActTableStyleName = "";
                foreach (XElement childNode in tablePrNode.Nodes())
                {
                    if (childNode.Name == ns + "tblStyle")
                    {
                        ActTableStyleName = childNode.Attribute(ns + "val").Value;
                        if (!tblList.Any(s => ActTableStyleName.Contains(s)))
                        {
                            tblList.Add(ActTableStyleName); //xmlstyleID
                            if (!TableViewList.Keys.Contains(ActTableStyleName))
                            { 
                                TableViewList.Add(ActTableStyleName, new List<TableView>());
                            }
                        }
                    }

                    if ((childNode.Name == ns + "tblLook"))
                    {
                        viewInd++;
                        TableView tempView = new TableView();
                        tempView.ViewName = "TableView" + viewInd;
                        foreach (XAttribute xat in childNode.Attributes())
                        {
                            if (xat.Name == ns + "firstRow") { tempView.HeaderRow = xat.Value; }
                            if (xat.Name == ns + "lastRow") { tempView.TotalRow = xat.Value; }
                            if (xat.Name == ns + "firstColumn") { tempView.FirstColumn = xat.Value; }
                            if (xat.Name == ns + "lastColumn") { tempView.LastColumn = xat.Value; }
                            if (xat.Name == ns + "noHBand") { tempView.BandedRows = xat.Value; }
                            if (xat.Name == ns + "noVBand") { tempView.BandedColumns = xat.Value; }
                            
                        }
                        TableViewList[ActTableStyleName].Add(tempView);
                    }

                }

            }

            #endregion

            #region Settings Part
            XDocument ExtractedSettings = ExtractPart(filenamewpath, "settings", false);
            ns = ExtractedSettings.Root.GetNamespaceOfPrefix("w");

            if (ExtractedSettings != null)
                ProcessElements(ExtractedSettings.Root, JsonrootElement.FileDefinitionPart, O2IDictionary.settings, null, new List<ExpandoObject>());

            #endregion

            #region Numbering Extract
            resultBox.AppendText("Extracting numberings..." + Environment.NewLine);
            XDocument ExtractedNumbering = ExtractPart(filenamewpath, "numbering", false);
            JsonList = new List<ExpandoObject>();


            bool LinkedListTemplateFound = false;

            if (ExtractedNumbering != null)
            {
                ns = ExtractedNumbering.Root.GetNamespaceOfPrefix("w");

                foreach (XElement rootElement in ExtractedNumbering.Root.Elements(ns + "abstractNum"))
                {

                    foreach (XElement listlevel in rootElement.Elements(ns + "lvl").Elements(ns + "pStyle"))
                    {
                        LinkedListTemplateFound = true;
                    }
                    if (LinkedListTemplateFound)
                    {
                        ProcessElements(rootElement, JsonrootElement.FileDefinitionPart, O2IDictionary.numbering, null, JsonList);
                        LinkedListTemplateFound = false;
                    }

                }
            }
           
            int i = 0;
            foreach (dynamic _ListTemplate in JsonList)
            {
                
               // dynamic _ListTemplate = _ListTemplate1;
                
                if (_ListTemplate.multiLevelType == "OutlineNumbered" || _ListTemplate.multiLevelType == "hybridmulti")
                {

                    i = 0;
                    List<string> NumberedStyles = new List<string>(); 
                    List<string> BulletedStyles = new List<string>();

                    IDictionary<string, Object> _listLevel = _ListTemplate.ListLevel[i];
                    
                    while (_listLevel.ContainsKey("LinkedStyle") &&
                                    (_ListTemplate.ListLevel[i].NumberStyle == "wdListNumberStyleBullet"))
                    {

                        if (i == 0)
                        {
                            BulletedStyles.Add("BulletedList");
                        }
                        BulletedStyles.Add(_ListTemplate.ListLevel[i].LinkedStyle);
                        i++;

                        if (i > 4)
                            break;
                        _listLevel = _ListTemplate.ListLevel[i];
                    }
                    
                    if (i > 1)
                        Lists.Add(BulletedStyles);


                    i = 0;
                    while (_listLevel.ContainsKey("LinkedStyle") &&
                        (_ListTemplate.ListLevel[i].NumberStyle != "wdListNumberStyleBullet"))
                    {
                        
                        if (i == 0)
                        {
                            NumberedStyles.Add("NumberedList");
                        }
                        NumberedStyles.Add(_ListTemplate.ListLevel[i].LinkedStyle);
                        i++;
                        if (i > 4)
                            break;
                        _listLevel = _ListTemplate.ListLevel[i];
                        
                    }

                    if (i > 1)
                        Lists.Add(NumberedStyles);
                }

            }
            #endregion

            #endregion

            #region Theme Extract

            XDocument ExtractedThemes = ExtractPart(filenamewpath, "theme", false);
            ns = ExtractedThemes.Root.GetNamespaceOfPrefix("a");
            resultBox.AppendText("Extracting color scheme..." + Environment.NewLine);
            XElement ColorScheme = ExtractedThemes.Root.Element(ns + "themeElements").Element(ns + "clrScheme");
            resultBox.AppendText("Extracting font scheme..." + Environment.NewLine);
            XElement FontScheme = ExtractedThemes.Root.Element(ns + "themeElements").Element(ns + "fontScheme");
            resultBox.AppendText("Extracting effects scheme..." + Environment.NewLine);
            XElement EffectScheme = ExtractedThemes.Root.Element(ns + "themeElements").Element(ns + "fmtScheme");
            string XMLDeclaration = ExtractedThemes.Declaration.ToString();

            if (Directory.Exists(path)) Directory.Delete(path, true);
            Directory.CreateDirectory(path);


            File.WriteAllText(path + @"\color.theme", XMLDeclaration + Environment.NewLine + ColorScheme.ToString());
            File.WriteAllText(path + @"\font.theme", XMLDeclaration + Environment.NewLine + FontScheme.ToString());
            File.WriteAllText(path + @"\effect.theme", XMLDeclaration + Environment.NewLine + EffectScheme.ToString());

            #endregion

            #region StyleExtract

            resultBox.AppendText("Extracting styles..." + Environment.NewLine);

            XDocument ExtractedStyles = ExtractPart(filenamewpath, "styles", false);
            ns = ExtractedStyles.Root.GetNamespaceOfPrefix("w");

            JsonList = new List<ExpandoObject>();

      
            if (ExtractedStyles != null)
            {
                JsonrootElement.StyleDefinitionPart = new ExpandoObject();
                int ind = 1;

                #region Font and Paragraph Defaults
                if (ExtractedStyles.Root.Element(ns + "docDefaults") != null)
                {
                    ProcessElements(ExtractedStyles.Root.Element(ns + "docDefaults"), JsonrootElement.FileDefinitionPart, O2IDictionary.docDefaults, null, new List<ExpandoObject>());
                }
                #endregion

                foreach (XElement rootElement in ExtractedStyles.Root.Elements(ns + "style"))
                {

                    if (EnableStyleToExtract(rootElement.Element(ns + "name").Attribute(ns + "val").Value, rootElement, ns))
                    {
                        //LogToFile("***************Processing style: " + rootElement.Attribute(ns + "styleId").Value + "******************");
                        ProcessElements(rootElement, JsonrootElement.StyleDefinitionPart, O2IDictionary.style, null, JsonList);

                        StyleNameInfo _StyleNameInfo = new StyleNameInfo();

                        _StyleNameInfo.StyleIndex = ind.ToString();
                        _StyleNameInfo.XmlName = rootElement.Element(ns + "name").Attribute(ns + "val").Value;
                        _StyleNameInfo.XmlStyleID = rootElement.Attribute(ns + "styleId").Value;
                        _StyleNameInfo.BuiltinStyleName = JsonList[ind - 1].FirstOrDefault(x => x.Key == "BuiltinStyleName").Value.ToString();
                        if (_StyleNameInfo.BuiltinStyleName.Contains("Accent") == true)
                        {
                            _StyleNameInfo.BuiltinStyleName = _StyleNameInfo.BuiltinStyleName.Replace(" Accent", " - Accent");
                        }
                        //_StyleNameInfo.XmlStyleID_BaseStyle = rootElement.Element(ns + "name")
                        _StyleNameInfo.PreferredStyle = "Not Applicable";
                        _StyleNameInfo.StyleDescription = "Not Applicable";

                        // if (_StyleNameInfo.BuiltinStyleName.Substring(0, 2) != "wd") _StyleNameInfo.BuiltinStyleName = JsonList; 

                        _StyleDicList.StyleNameList.Add(_StyleNameInfo);

                        //Dropdown list
                        if (rootElement.Attribute(ns+"type").Value == "paragraph" || rootElement.Attribute(ns + "type").Value == "character") stlList.Add(_StyleNameInfo.XmlName);

                        ind++;
                    }
                }
            }
            #endregion



            //JsonrootElement.StyleDefinitions;
            // 
            //File.WriteAllText(path + @"\StyleListDic.txt", JsonConvert.SerializeObject(_StyleDicList, Newtonsoft.Json.Formatting.Indented));

           // WriteConfig(path);    
            resultBox.AppendText("Completed." + Environment.NewLine);
            resultBox.ScrollToEnd();
        }

        public List<string> InheritanceList(List<string> SelectedStyles)
        {
            string BaseStyle;
            List<string> StylesToExport = new List<string>();
        
            foreach (string StyleName in SelectedStyles)
            {
                BaseStyle = StyleName;                

                while (BaseStyle != "")
                {
                    IDictionary<string, Object> _Style = JsonrootElement.StyleDefinitionPart.StyleSettings[Convert.ToInt32(_StyleDicList.StyleNameList.FirstOrDefault(x => x.XmlStyleID == BaseStyle).StyleIndex)-1];

                    if (_Style.ContainsKey("BaseStyle"))
                    {    
                        if (!StylesToExport.Contains(BaseStyle)) StylesToExport.Add(BaseStyle);
                        BaseStyle = _Style.FirstOrDefault(x => x.Key == "BaseStyle").Value.ToString();
                    }
                    else
                    {
                        if (!StylesToExport.Contains(BaseStyle)) StylesToExport.Add(BaseStyle);
                        BaseStyle = "";
                    }
                }
            }

            return StylesToExport;
        }

        public void RemoveStylesFromJSON(List<string> SelectedStyles)
        {
            List<string> StylesToInclude = InheritanceList(SelectedStyles);

            int i = 0;
            while (i < JsonrootElement.StyleDefinitionPart.StyleSettings.Count)
            {
                if (JsonrootElement.StyleDefinitionPart.StyleSettings[i].StyleType.ToString() == "wdStyleTypeTable" &&
                       !StylesToInclude.Contains(JsonrootElement.StyleDefinitionPart.StyleSettings[i].StyleID))
                {
                    List<ExpandoObject> _styles = JsonrootElement.StyleDefinitionPart.StyleSettings;
                    _StyleDicList.StyleNameList.RemoveAt(i);
                    _styles.RemoveAt(i);

                }
                else { i++; }                    
            }
            int index = 1;
            foreach (dynamic tmpStl in _StyleDicList.StyleNameList)
            {
                tmpStl.StyleIndex = index.ToString();
                index++;
            }

        }
        public void WriteConfig(string path)
        {
            string DicJson = JsonConvert.SerializeObject(JsonrootElement, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(path + @"\config.txt",  EncryptFile(DicJson));
            //File.WriteAllText(path + @"\config_notEncrypted.txt", DicJson);

        }
        public static XDocument ExtractPart(string fileName, string part2extract, bool getStylesWithEffectsPart = true)
        {
            XDocument part = null;

            using (var document = WordprocessingDocument.Open(fileName, false))
            {
                var docPart = document.MainDocumentPart;
                if (part2extract == "styles")
                {
                    StylesPart stylesPart = null;
                    if (getStylesWithEffectsPart)
                        stylesPart = docPart.StylesWithEffectsPart;
                    else
                        stylesPart = docPart.StyleDefinitionsPart;

                    if (stylesPart != null)
                    {
                        using (var reader = XmlNodeReader.Create(stylesPart.GetStream(FileMode.Open, FileAccess.Read)))
                        {
                            part = XDocument.Load(reader);
                        }
                    }
                }
                if (part2extract == "numbering")
                {
                    NumberingDefinitionsPart numberingPart = null;
                    numberingPart = docPart.NumberingDefinitionsPart;
                    if (numberingPart != null)
                    {
                        using (var reader = XmlNodeReader.Create(numberingPart.GetStream(FileMode.Open, FileAccess.Read)))
                        {
                            part = XDocument.Load(reader);
                        }
                    }
                }
                if (part2extract == "settings")
                {
                    DocumentSettingsPart settingsPart = null;
                    settingsPart = docPart.DocumentSettingsPart;
                    if (settingsPart != null)
                    {
                        using (var reader = XmlNodeReader.Create(settingsPart.GetStream(FileMode.Open, FileAccess.Read)))
                        {
                            part = XDocument.Load(reader);
                        }
                    }
                }
                if (part2extract == "theme")
                {
                    ThemePart themePart = null;
                    themePart = docPart.ThemePart;
                    if (themePart != null)
                    {
                        using (var reader = XmlNodeReader.Create(themePart.GetStream(FileMode.Open, FileAccess.Read)))
                        {
                            part = XDocument.Load(reader);
                        }
                    }
                }

                if (part2extract == "document")
                {
                    if (docPart != null)
                    {
                        using (var reader = XmlNodeReader.Create(docPart.GetStream(FileMode.Open, FileAccess.Read)))
                        {
                            part = XDocument.Load(reader);
                        }
                    }
                }
            }
            return part;
        }

        public static bool EnableStyleToExtract(string StyleXmlName, XElement rootElement, XNamespace ns)
        {
            if (StyleXmlName.Length > 4)
            if (StyleXmlName.Substring(StyleXmlName.Length - 4) == "Char" && rootElement.Attribute(ns + "type").Value == "character" && rootElement.Elements(ns + "link").Any())
            {
                return false;
            }
            return true;
        }

        public static void ProcessElements(XElement ActualElement, ExpandoObject JsonElement, dynamic O2IDictionaryPart, string OldSequenceValue, List<ExpandoObject> JsonList)
        {

            var xJsonElement = (IDictionary<string, Object>)JsonElement;
             //LogToFile("Processing ActualElement: " + ActualElement.Name.LocalName);
            bool SequenceElement;
            string SequenceValue = null;
            string ActualElementSequenceValue = null;
            string ElementInteropValue = O2IDictionaryPart.interop;
            //LogToFile("Interop name of the Actual XElement(ElementInteropValue): " + ElementInteropValue);
            bool Elementskipped = false;
            dynamic NextJsonElement = new ExpandoObject();

            var xNextJsonElement = (IDictionary<string, Object>)NextJsonElement;

            if (O2IDictionaryPart.GetValue("sequence") != null)
            {
                SequenceElement = true;
                SequenceValue = O2IDictionaryPart.GetValue("sequence");
                ActualElementSequenceValue = SequenceValue;
            }
            else
            {
                SequenceElement = false;
            }

            //LogToFile("List(SequenceElement): " + SequenceElement.ToString());

            if (SequenceElement)
            {
                JsonList.Add(NextJsonElement);
                ////LogToFile("Adding next JSON node to List");
            }
            else
            {
                if (ElementInteropValue != "")
                {
                    xJsonElement.Add(ElementInteropValue, xNextJsonElement);
                    ////LogToFile("Adding next JSON node to Object");
                }
                else
                {
                    Elementskipped = true;
                    // //LogToFile("Skipping main element");
                }
            }

            ////LogToFile("Checking attributes...");
            if (ActualElement.HasAttributes)

                if (!ExcludedAttributes.Any(s => ActualElement.Name.LocalName.Contains(s)))  
                   // ActualElement.Name.LocalName.ToString() != "settings" && ActualElement.Name.LocalName.ToString() != "sectPr")
                {
                    ////LogToFile("Actual Element has attributes");
                    foreach (XAttribute Xat in ActualElement.Attributes())
                    {

                        //LogToFile("Processing attribute: " + ActualElement.Name.LocalName + @"\@" + Xat.Name.LocalName);
                        dynamic AttributeInterop = O2IDictionaryPart.GetValue("@" + Xat.Name.LocalName);
                        dynamic AttributeInteropValue;
                        if (AttributeInterop.ToString() == "" || AttributeInterop.interop == "")
                        {
                            AttributeInterop = O2IDictionaryPart;
                            AttributeInteropValue = Xat.Value;
                            xJsonElement[AttributeInterop.interop.ToString()] = AttributeInteropValue.ToString();
                        }
                        else
                        {
                            AttributeInteropValue = AttributeInterop.GetValue(Xat.Value);
                            if (AttributeInteropValue == null) AttributeInteropValue = Xat.Value;
                            if (Elementskipped)
                            {
                                xJsonElement.Add(AttributeInterop.interop.ToString(), AttributeInteropValue.ToString());
                            }
                            else
                            {
                                xNextJsonElement.Add(AttributeInterop.interop.ToString(), AttributeInteropValue.ToString());
                            }
                        }
                    }
                    //LogToFile("Building JSON object: \r\n Attribute:" + AttributeInterop.interop.ToString() + "\r\n Value: " + AttributeInteropValue.ToString());
                
                }
                else
                {
                    ////LogToFile("Actual Element has NO attributes");
                }

            ////LogToFile("Checking child elements...");
            if (ActualElement.HasElements)
            {
                List<ExpandoObject> NextJsonList = new List<ExpandoObject>();
                ////LogToFile("Actual Element has child element(s)");
                foreach (XElement ChildElement in ActualElement.Elements())
                {
                    //LogToFile("Processing child element(s): " + ActualElement.Name.LocalName + @"\" + ChildElement.Name.LocalName);
                    dynamic NextO2IDictionaryPart = O2IDictionaryPart.GetValue(ChildElement.Name.LocalName);
                    if (NextO2IDictionaryPart != null && !ExcludedElements.Any(s => ChildElement.Name.LocalName.Contains(s)))
                    //if (!ExcludedElements.Any(s => ChildElement.Name.LocalName.Contains(s)))
                    {
                        //dynamic NextO2IDictionaryPart = O2IDictionaryPart.GetValue(ChildElement.Name.LocalName);
                        string nextSequenceValue = NextO2IDictionaryPart.GetValue("sequence");

                        if (ActualElementSequenceValue != null && nextSequenceValue != null)
                        {
                            ProcessElements(ChildElement, NextJsonElement, NextO2IDictionaryPart, SequenceValue, NextJsonList);
                        }
                        else if ((SequenceValue == nextSequenceValue && nextSequenceValue != null) || (SequenceValue == null && nextSequenceValue != null))
                        {
                            ProcessElements(ChildElement, NextJsonElement, NextO2IDictionaryPart, SequenceValue, JsonList);
                        }
                        else if (nextSequenceValue != SequenceValue && nextSequenceValue != null)
                        {
                            JsonList = new List<ExpandoObject>();
                            ProcessElements(ChildElement, NextJsonElement, NextO2IDictionaryPart, SequenceValue, JsonList);
                        }
                        else
                        {
                            ProcessElements(ChildElement, NextJsonElement, NextO2IDictionaryPart, SequenceValue, new List<ExpandoObject>());
                        }
                        SequenceValue = NextO2IDictionaryPart.GetValue("sequence");

                    }
                    else
                    {
                        // //LogToFile("Element skipped");
                    }
                };
            }
            else
            {
                ////LogToFile("ActualElement has NO child element.");
            }
            if (!ActualElement.HasAttributes && !ActualElement.HasElements)
            {
                xJsonElement[ElementInteropValue] = "1";
            }
            if (SequenceElement)
            {
                xJsonElement[ElementInteropValue] = JsonList;
            }

        }
        public string EncryptFile(string plainContent)
        {
            
            string key = Settings.Default.Config + Settings.Default.ApplicationName;
            if (key.Length > 8)
                key = key.Substring(0, 8);
            else
                key = (key + "!@#$%^&*").Substring(0, 8);
            byte[] plainContentbytes = Encoding.UTF8.GetBytes(plainContent);
            using (var DES = new DESCryptoServiceProvider())
            {
                DES.IV = Encoding.UTF8.GetBytes(key);
                DES.Key = Encoding.UTF8.GetBytes(key);
                DES.Mode = CipherMode.CBC;
                DES.Padding = PaddingMode.PKCS7;


                using (var memStream = new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memStream, DES.CreateEncryptor(), CryptoStreamMode.Write);

                    cryptoStream.Write(plainContentbytes, 0, plainContentbytes.Length);
                    cryptoStream.FlushFinalBlock();
                    memStream.Position = 0;

                    return Convert.ToBase64String(memStream.ToArray());
                }
            }
        }

       
        public class StyleDicList
        {
            public IList<StyleNameInfo> StyleNameList { get; set; }
        }

        public class StyleNameInfo
        {
            public string StyleIndex { get; set; }
            public string XmlStyleID { get; set; }
            public string XmlName { get; set; }
            public string BuiltinStyleName { get; set; }
            //public string XmlStyleID_BaseStyle { get; set; }
            public bool IsImported { get; set; }
            public string PreferredStyle { get; set; }
            public string StyleDescription { get; set; }
        }

        public class TableView
        {
            public string ViewName { get; set; }
            public string HeaderRow { get; set; }
            public string FirstColumn { get; set; }
            public string TotalRow { get; set; }
            public string LastColumn { get; set; }
            public string BandedRows { get; set; }
            public string BandedColumns { get; set; }

        }

        public class CaptionConfig
        {
            public string CaptionType { get; set; }
            public List<string> CaptionLabels { get; set; }
            public string Position { get; set; }
            public string NumberStyle { get; set; }
            public bool IncludeChapterNumber { get; set; }
            public string Separator { get; set; }
            public int ChapterStyleLevel { get; set; }
            public string CaptionStyle { get; set; }
            public string TableContentStyle { get; set; }
        }

    }
}
