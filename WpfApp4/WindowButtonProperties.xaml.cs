﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace WpfApp4
{
    /// <summary>
    /// Interaction logic for WindowButtonProperties.xaml
    /// </summary>
    public partial class WindowButtonProperties : Window
    {
        Button btnStlButtonActive;
        
        List<string> StyleList;
        List<string> ButtonData = new List<string>();
        Icons iconWindow;
        protected override void OnSourceInitialized(EventArgs e)
        {
            IconHelper.RemoveIcon(this);
        }
        public WindowButtonProperties(Button btnStlButtonActive,  List<string> StyleList)
        {
            InitializeComponent();
            
            this.btnStlButtonActive = btnStlButtonActive;
            
            this.StyleList = StyleList;
            ButtonData = (List<string>)btnStlButtonActive.Tag;
            btnName.Text = ButtonData[1];
            stlDropDown.ItemsSource = StyleList;
            if (ButtonData[0] != "")
            {
                if (!StyleList.Contains(ButtonData[0])) StyleList.Add(ButtonData[0]);
                stlDropDown.SelectedItem = ButtonData[0];
            }
            else
            {
                stlDropDown.SelectedItem = "Select Style";
            }
            StyleList.Sort();
            if (ButtonData[7] != "")
                img_help.ToolTip = ButtonData[7];
            
             if (ButtonData[2] != "")
                btnStlButton.Content = new Image()
                {
                    Source = new BitmapImage(new Uri(Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config)+ButtonData[2], UriKind.Absolute)),
                };

            if (btnStlButton.Content != null && stlDropDown.SelectedItem != null)
            {
                btnAssign.IsEnabled = true;
            }
            btnStlButton.Tag = ButtonData[2];
            tbSuperToolT.Text = ButtonData[3];
            tbToolT.Text = ButtonData[4];
            tbKeyT.Text = ButtonData[5];
            if (btnName.Text != "")
            {
                btnAssign.Content = "Update";
                btnDelete.IsEnabled = true;
            }
            
        }

        private void btnAssign_Click(object sender, RoutedEventArgs e)
        {
           
            ButtonData[0] = stlDropDown.SelectedValue.ToString();
            ButtonData[1] = (btnName.Text);
            ButtonData[2] = (btnStlButton.Tag.ToString());
            ButtonData[3] = (tbSuperToolT.Text);
            ButtonData[4] = (tbToolT.Text);
            ButtonData[5] = (tbKeyT.Text);
            if (ButtonData[6] != "")
            {
                ButtonData[6] = ButtonData[6];
            }
            else
            {
                ButtonData[6] = "";
            }
            btnStlButtonActive.Tag = ButtonData;
            btnStlButtonActive.Content = new Image()
            {
                Source = new BitmapImage(new Uri(Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config)+btnStlButton.Tag.ToString(), UriKind.Absolute))
            };
            if (btnName.Text == "") btnStlButtonActive.IsEnabled = false;
            StyleList.Remove(stlDropDown.SelectedItem.ToString());
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (btnStlButtonActive.IsEnabled)
            {
                if (ButtonData[0] != "")
                {
                    if (StyleList.Contains(ButtonData[0])) StyleList.Remove(ButtonData[0]);
                    
                }
            };
        }

        private void openDialogIcon_Click(object sender, RoutedEventArgs e)
        {
           iconWindow = new Icons(btnStlButton);
           iconWindow.ShowDialog();
            
            if (btnStlButton.Content!=null && stlDropDown.SelectedItem != null)
            {
                btnAssign.IsEnabled = true;
            }
        }

        private void stlDropDown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (btnStlButton.Content != null && stlDropDown.SelectedItem != null)
            {
                btnAssign.IsEnabled = true;
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (ButtonData[0] != "")
            {
                if (!StyleList.Contains(ButtonData[0])) StyleList.Add(ButtonData[0]);
                ButtonData[0] = "";
                btnStlButtonActive.Tag = ButtonData;
            }
            btnStlButtonActive.IsEnabled = false;
            Close();
        }

        private void Window_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
    public class ButtonConfigList
    {
        public IList<ButtonConfigObject> ButtonConfigs { get; set; }
    }

    public class ButtonConfigObject
    {
        public string ButtonLabel { get; set; }
        public string ButtonID { get; set; }
        public string AssignedPicturePath { get; set; }
        public string SuperToolTip { get; set; }
        public string ToolTip { get; set; }
        public string KeyTip { get; set; }
        public string AttachedStyle { get; set; }
    }
    static class IconHelper
    {
        [DllImport("user32.dll")]
        static extern int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hwnd, int index, int newStyle);

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter,
                   int x, int y, int width, int height, uint flags);

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hwnd, uint msg,
                   IntPtr wParam, IntPtr lParam);

        const int GWL_EXSTYLE = -20;
        const int WS_EX_DLGMODALFRAME = 0x0001;
        const int SWP_NOSIZE = 0x0001;
        const int SWP_NOMOVE = 0x0002;
        const int SWP_NOZORDER = 0x0004;
        const int SWP_FRAMECHANGED = 0x0020;
        const uint WM_SETICON = 0x0080;

        public static void RemoveIcon(Window window)
        {
            // Get this window's handle
            IntPtr hwnd = new WindowInteropHelper(window).Handle;

            // Change the extended window style to not show a window icon
            int extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_DLGMODALFRAME);

            // Update the window's non-client area to reflect the changes
            SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_NOMOVE |
                  SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
        }
    }
}
