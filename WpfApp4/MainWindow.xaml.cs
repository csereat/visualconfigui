﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Media;
using System.Text.RegularExpressions;

namespace WpfApp4
{
    public partial class MainWindow : System.Windows.Window
    {
        List<List<string>> ActiveButtonList;
        List<List<string>> PassiveButtonList;
        List<string> StyleList = new List<string>();
        List<string> TableList = new List<string>();
        List<List<string>> ListStyles = new List<List<string>>();
        
        List<ButtonConfigObject> BCOList = new List<ButtonConfigObject>();
        ExtractStyles Extract = new ExtractStyles();
        string path;
        WindowButtonProperties WBP;
        ExtractStyles.StyleDicList _StyleDicList;
        public MainWindow()
        {
            InitializeComponent();
            CenterLocation();
            this.Width = 375;
            this.Height = 300;
            grd_ButtonConfig.Visibility = Visibility.Hidden;
            ActiveButtonList = new List<List<string>>();
            PassiveButtonList = new List<List<string>>();

            for (int col = 0; col < 9; col++)
            {
                for (int row = 0; row < 3; row++)
                {
                    List<string> sbtpos = new List<string>()
                    { "btn_SB" + (col * 3 + row + 1),col.ToString(),row.ToString()};
                    ActiveButtonList.Add(sbtpos);
                }
            }
            for (int col = 0; col < 4; col++)
            {
                for (int row = 0; row < 4; row++)
                {
                    List<string> sbtpos = new List<string>()
                    {"passive" + PassiveButtonList.Count.ToString(), col.ToString(),row.ToString()};

                    PassiveButtonList.Add(sbtpos);

                }
            }

        }

        private void CenterLocation()
        {
            //center this message window w/r to the WorkArea (i.e the screen)
            this.Top = (SystemParameters.WorkArea.Height - this.Height) / 2;
            this.Left = (SystemParameters.WorkArea.Width - this.Width / 2) / 2;
            //
        }

        public static void LogToFile(string msg)
        {
            StreamWriter sw = File.AppendText(@"c:\temp\ooxmlprocesslog.txt");
            try
            {
                string logLine = string.Format(
                    "{0}: {1}", DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
        OpenFileDialog openFileDialog = new OpenFileDialog();

        string filenamewpath;

        private void OpenToExtract_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Word Documents(*.docx,*dotx) | *.docx;*dotx";
            openFileDialog.Title = "Please select a Word document from which you would like to create a Visual Config...";
            openFileDialog.ShowDialog();
            filenamewpath = openFileDialog.FileName;
            OpenToExtract.ToolTip = filenamewpath;

            

            if (!string.IsNullOrEmpty(filenamewpath))
            {
                OpenToExtract_TextBlock.HorizontalAlignment = HorizontalAlignment.Left;
                
                if (filenamewpath.Length > 47) //approx 295 pixel
                {
                    OpenToExtract_TextBlock.Text = openFileDialog.SafeFileName;
                    OpenToExtract.Width = openFileDialog.SafeFileName.Length * 6.3;

                    if (OpenToExtract.Width > 295)
                    {
                        OpenToExtract_TextBlock.TextTrimming = TextTrimming.CharacterEllipsis;
                        OpenToExtract.Width = 295;
                    }
                }
                else
                {
                    OpenToExtract_TextBlock.Text = filenamewpath;
                    OpenToExtract.Width = filenamewpath.Length * 6.3;
                }
            }

            if (!string.IsNullOrEmpty(filenamewpath) && txb_ConfigName.Text != "")
                btnExtract.IsEnabled = true;
        }

        private void Extract_Click(object sender, RoutedEventArgs e)
        {
            
            if (txb_ConfigName.Text.All(Char.IsLetterOrDigit))
            {
                txb_ConfigName.Text = txb_ConfigName.Text + ".1";
                List<string> PredefinedButtons;
                List<string> PredefinedButtonsHelp;
                List<string> StylesToBeRemved = new List<string>();
                Properties.Settings.Default.DefaultPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Properties.Settings.Default.ApplicationName);
                Properties.Settings.Default.ConfigPath = Path.Combine(Properties.Settings.Default.DefaultPath, Properties.Settings.Default.ConfigFolderName);
                Properties.Settings.Default.Config = txb_ConfigName.Text;

                path = Path.Combine(Properties.Settings.Default.ConfigPath, Properties.Settings.Default.Config);
                bool ConfigFolderExists = false;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);                   
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("VC with this name already exists would you like to overwrite it ?", "OverWrite ?", MessageBoxButton.YesNo);
                    if (result == MessageBoxResult.No)
                    {
                        return;
                    }
                    ConfigFolderExists = true;
                }

                
                _StyleDicList = new ExtractStyles.StyleDicList()
                {
                    StyleNameList = new List<ExtractStyles.StyleNameInfo>()
                };
                Properties.Settings.Default.Config = txb_ConfigName.Text;
                try
                {
                    Extract.extractStyles(filenamewpath, resultBox, StyleList, TableList, ListStyles, txb_ConfigName.Text, _StyleDicList);
                    txb_ConfigName.IsEnabled = false;
                    btnExtract.IsEnabled = false;
                    OpenToExtract.IsEnabled = false;
                    OpenToExtract_TextBlock.Foreground = Brushes.Gray;

                    this.Height = 700;
                    grd_ButtonConfig.Visibility = Visibility.Visible;
                    TableListFunction(TableList);
                    PredefinedButtons = new List<string>();
                    PredefinedButtonsHelp = new List<string>();
                    PredefinedButtons.Add("heading 1");
                    PredefinedButtonsHelp.Add("Please select a style for Heading 1." + Environment.NewLine + "This style will be used for top most level heading.");
                    PredefinedButtons.Add("heading 2");
                    PredefinedButtonsHelp.Add("Please select a style for Heading 2." + Environment.NewLine + "This style will be used as a subheading of Heading 1.");
                    PredefinedButtons.Add("heading 3");
                    PredefinedButtonsHelp.Add("Please select a style for Heading 3." + Environment.NewLine + "This style will be used as a subheading of Heading 2.");
                    PredefinedButtons.Add("heading 4");
                    PredefinedButtonsHelp.Add("Please select a style for Heading 4." + Environment.NewLine + "This style will be used as a subheading of Heading 3.");
                    PredefinedButtons.Add("heading 5");
                    PredefinedButtonsHelp.Add("Please select a style for Heading 5." + Environment.NewLine + "This style will be used as a subheading of Heading 4.");
                    PredefinedButtons.Add("Instruction");
                    PredefinedButtonsHelp.Add("Please select a style for Instruction." + Environment.NewLine + "This style will be used to write text that can be later on hidden or deleted" + Environment.NewLine + "with one click of a button.");
                    PredefinedButtons.Add("NOTTOC");
                    PredefinedButtonsHelp.Add("Please select a style for Not TOC Heading." + Environment.NewLine + "This style will be used to create Headings which are not listed in your TOC.");
                    PredefinedButtons.Add("TOCEnt");
                    PredefinedButtonsHelp.Add("Please select a style for TOC Entry." + Environment.NewLine + "Lines written with this style are going to be included in your TOC.");
                    PredefinedButtons.Add("Body Text");
                    PredefinedButtonsHelp.Add("Please select a style for Plain text." + Environment.NewLine + "This style will be used for writing the most part of your documents." + Environment.NewLine + "It is advisable to have a style for your plain text and not to use Normal for this purpose.");

                    foreach (string STyleName in StyleList)
                    {
                        if (PredefinedButtons.Contains(STyleName))
                        {
                            StyleButton tmpbtn = new StyleButton();
                            StylesToBeRemved.Add(STyleName);
                            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WpfApp4.Resources." + STyleName + ".png");
                            if (!Directory.Exists(path + @"\Icons"))
                            {
                                Directory.CreateDirectory(path + @"\Icons");
                            }
                            FileStream fileStream = new FileStream(path + @"\Icons\" + STyleName + ".png", FileMode.Create);
                            for (int j = 0; j < stream.Length; j++)
                                fileStream.WriteByte((byte)stream.ReadByte());
                            fileStream.Close();
                            stream.Close();

                            //button Name
                            tmpbtn.Name = ActiveButtonList[0][0];

                            //button image
                            tmpbtn.Content = new Image()
                            {
                                Source = new BitmapImage(new Uri(path + @"\Icons\" + STyleName + ".png", UriKind.Absolute)),
                                VerticalAlignment = VerticalAlignment.Center
                            };

                            //superToolTip

                            

                            //button Tag
                            List<string> ButtonData = new List<string>
                        {
                            STyleName,
                            tmpbtn.Name,
                            @"\Icons\" + STyleName + ".png",
                            "SuperTipp Jo Hosszu szoveg esetleg link http:\\akarki.hu",
                            STyleName.Substring(0,1).ToUpper() + STyleName.Substring(1),
                            "",
                            STyleName,
                            PredefinedButtonsHelp[PredefinedButtons.IndexOf(STyleName)]
                        };
                            tmpbtn.Tag = ButtonData;

                            //button click
                            tmpbtn.Click += new RoutedEventHandler(stlbutton_click);

                            //button set col and row
                            Grid.SetColumn(tmpbtn, Convert.ToInt32(ActiveButtonList[0][1]));
                            Grid.SetRow(tmpbtn, Convert.ToInt32(ActiveButtonList[0][2]));
                            smallGrid.Children.Add(tmpbtn);
                            PredefinedButtonsHelp.RemoveAt(PredefinedButtons.IndexOf(STyleName));
                            PredefinedButtons.Remove(STyleName);
                            ActiveButtonList.RemoveAt(0);
                        }
                    }
                    int row = 0;
                    foreach (string leftout in PredefinedButtons)
                    {
                        if (!Directory.Exists(path + @"\Icons"))
                        {
                            Directory.CreateDirectory(path + @"\Icons");
                        }
                        StyleButton tmpbtn = new StyleButton();
                        Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WpfApp4.Resources." + leftout + ".png");
                        FileStream fileStream = new FileStream(path + @"\Icons\" + leftout + ".png", FileMode.Create);
                        for (int j = 0; j < stream.Length; j++)
                            fileStream.WriteByte((byte)stream.ReadByte());
                        fileStream.Close();
                        stream.Close();

                        //button Image
                        tmpbtn.Content = new Image()
                        {
                            Source = new BitmapImage(new Uri(path + @"\Icons\" + leftout + ".png", UriKind.Absolute)),
                            VerticalAlignment = VerticalAlignment.Center
                        };

                        //button Tag
                        List<string> ButtonData = new List<string>
                    {
                        "",
                        tmpbtn.Name,
                        @"\Icons\" + leftout + ".png",
                        "",
                        "",
                        "",
                        leftout,
                        PredefinedButtonsHelp[PredefinedButtons.IndexOf(leftout)]
                    };
                        tmpbtn.Tag = ButtonData;

                        //button Click
                        tmpbtn.Click += new RoutedEventHandler(stlbutton_click);
                        Grid.SetColumn(tmpbtn, Convert.ToInt32(PassiveButtonList[0][1]));
                        Grid.SetRow(tmpbtn, Convert.ToInt32(PassiveButtonList[0][2]));
                        leftoutGrid.Children.Add(tmpbtn);

                        //PredefinedButtonsHelp.RemoveAt(PredefinedButtons.IndexOf(leftout));
                        //PredefinedButtons.Remove(leftout);
                        //button Name
                        tmpbtn.Name = PassiveButtonList[0][0];
                        PassiveButtonList.RemoveAt(0);


                    }
                    foreach (string StyleName in StylesToBeRemved)
                    {
                        StyleList.Remove(StyleName);
                    }
                    RowDefinition rowdef = new RowDefinition();
                    rowdef.Height = new GridLength(50, GridUnitType.Pixel);
                    grdListStyles.RowDefinitions.Add(rowdef);
                    grdListStyles.ColumnDefinitions.Add(new ColumnDefinition());
                    Label lblListStyles = new Label()
                    {
                        Content = "The following Multilevel" + Environment.NewLine + "Lists were found, please" + Environment.NewLine + "select one of a kind:"
                    };
                    lblListStyles.FontSize = 10;
                    Grid.SetColumn(lblListStyles, 0);
                    Grid.SetRow(lblListStyles, 0);
                    grdListStyles.Children.Add(lblListStyles);
                    row = 1;
                    int listIndex = 0;
                    bool NumberedListSelected = false;
                    bool BulletedListSelected = false;

                    foreach (List<string> _listStyles in ListStyles)
                    {
                        string headingXMLID = _StyleDicList.StyleNameList.FirstOrDefault(x => x.BuiltinStyleName == "wdStyleHeading1").XmlStyleID;
                        if (!_listStyles[1].Contains(headingXMLID.Substring(0, headingXMLID.Length-1)))
                        {
                            rowdef = new RowDefinition();
                            rowdef.Height = new GridLength(20, GridUnitType.Pixel);
                            grdListStyles.RowDefinitions.Add(rowdef);

                            CheckBox ckbListStyle = new CheckBox()
                            {
                                Name = "chkBox" + row
                            };
                            ckbListStyle.Click += CkbListStyle_Click;

                            ckbListStyle.Content = ListStyles[listIndex][0];

                            //set checked the first bulleted and the first numbered lists
                            if ((ckbListStyle.Content.ToString() == "BulletedList") && !BulletedListSelected)
                            {
                                ckbListStyle.IsChecked = true;
                                BulletedListSelected = true;
                            }
                            if ((ckbListStyle.Content.ToString() == "NumberedList") && !NumberedListSelected)
                            {
                                ckbListStyle.IsChecked = true;
                                NumberedListSelected = true;
                            }
                            ckbListStyle.Tag = ListStyles[listIndex];
                            ckbListStyle.FontSize = 10;
                            Grid.SetColumn(ckbListStyle, 0);
                            Grid.SetRow(ckbListStyle, row);

                            ckbListStyle.MouseEnter += Rowdef_MouseEnter;
                            ckbListStyle.MouseLeave += Rowdef_MouseLeave;


                            grdListStyles.Children.Add(ckbListStyle);
                            row++;
                        }
                        listIndex++;
                    }

                }
                catch (Exception error)
                {
                    if (!ConfigFolderExists)
                    {
                        Directory.Delete(path, true);
                    }
                    resultBox.Text += error.Message;
                    resultBox.Text += Environment.NewLine;
                    resultBox.Text += "Visual Config Creation Failed due to an Error ....";

                }

            }
            else
            {
                MessageBox.Show("The configuration name specified is invalid! Please use only alphanumeric characters!");
            }
        }

        private void CkbListStyle_Click(object sender, RoutedEventArgs e)
        {
            CheckBox clickedchkBox = (CheckBox)sender;
            if (clickedchkBox.IsChecked == false)
            {
                clickedchkBox.IsChecked = false;
            }
            else
            {
                clickedchkBox.IsChecked = false;
                bool foundchecked = false;
                if (clickedchkBox.Content.ToString().Contains("Bulleted"))
                {
                    foreach (CheckBox ckb in grdListStyles.Children.OfType<CheckBox>())
                    {

                        if (ckb.IsChecked == true)
                        {
                            if (ckb.Content.ToString().Contains("Bulleted"))
                            {
                                foundchecked = true;
                                MessageBox.Show("Bulleted List already selected !");
                                break;
                            }
                        }
                        else
                        {
                            foundchecked = false;
                        }

                    }
                }
                if (clickedchkBox.Content.ToString().Contains("Numbered"))
                {
                    foreach (CheckBox ckb in grdListStyles.Children.OfType<CheckBox>())
                    {

                        if (ckb.IsChecked == true)
                        {
                            if (ckb.Content.ToString().Contains("Numbered"))
                            {
                                foundchecked = true;
                                MessageBox.Show("Numbered List already selected !");
                                break;
                            }
                        }
                        else
                        {
                            foundchecked = false;
                        }

                    }
                }
                if (!foundchecked)
                {
                    clickedchkBox.IsChecked = true;
                   
                }
            }
        }

        ListStyleDetails lsd;
        private void Rowdef_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            CheckBox chktmp = (CheckBox)sender;
            if (!chktmp.IsMouseOver)
                lsd.Hide();
        }

        private void Rowdef_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            CheckBox chktmp = (CheckBox)sender;
            List<string> chkTag = (List<string>)chktmp.Tag;
            lsd = new ListStyleDetails(chkTag);
            lsd.Top = chktmp.PointToScreen(new Point(0, 0)).Y + 20;
            lsd.Left = chktmp.PointToScreen(new Point(0, 0)).X + 80;
            lsd.Topmost = true;
            lsd.Show();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (ActiveButtonList.Count > 0)
            { 
                StyleButton tmpbtn = new StyleButton();

                //button Name
                tmpbtn.Name = ActiveButtonList[0][0];
                Grid.SetColumn(tmpbtn, Convert.ToInt32(ActiveButtonList[0][1]));
                Grid.SetRow(tmpbtn, Convert.ToInt32(ActiveButtonList[0][2]));

                ActiveButtonList.RemoveAt(0);
                ActiveButtonList.Sort(delegate (List<string> x, List<string> y)
                {
                    return Convert.ToUInt32(x[0].Substring(6)).CompareTo(Convert.ToUInt32(y[0].Substring(6)));
                });

                List<string> ButtonData = new List<string>
                            {
                                "",
                                tmpbtn.Name,
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            };
                tmpbtn.Tag = ButtonData;
                tmpbtn.Click += new RoutedEventHandler(stlbutton_click);
                smallGrid.Children.Add(tmpbtn);
            }

            if (ActiveButtonList.Count == 0)
            {
                addButton.IsEnabled = false;
            }
        }

        private void stlbutton_click(object sender, RoutedEventArgs e)
        {
            Button btnStlButtonClicked = (Button)sender;
            WBP = new WindowButtonProperties(btnStlButtonClicked, StyleList);
            WBP.Closing += WBP_Closing;
            WBP.ShowDialog();
            if (ActiveButtonList.Count>0)
            {
                addButton.IsEnabled = true;
            }
        }

        private void WBP_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (StyleButton sbt in smallGrid.Children)
            {
                if (!sbt.IsEnabled)
                {
                    sbt.IsEnabled = true;
                    List<string> sbtTag = (List<string>)sbt.Tag;
                    int col = Grid.GetColumn(sbt);
                    int row = Grid.GetRow(sbt);
                    List<string> sbtpos = new List<string>()
                    { sbt.Name,col.ToString(),row.ToString()};
                    ActiveButtonList.Add(sbtpos);
                    ActiveButtonList.Sort(delegate (List<string> x, List<string> y)
                    {
                        return Convert.ToUInt32(x[0].Substring(6)).CompareTo(Convert.ToUInt32(y[0].Substring(6)));
                    });
                    smallGrid.Children.Remove(sbt);
                    if (sbtTag[6] != "")
                    {
                        if (PassiveButtonList.Count>0)
                        {
                            Grid.SetColumn(sbt, Convert.ToInt32(PassiveButtonList[0][1]));
                            Grid.SetRow(sbt, Convert.ToInt32(PassiveButtonList[0][2]));
                            sbt.Name = PassiveButtonList[0][0];
                            PassiveButtonList.RemoveAt(0);
                            PassiveButtonList.Sort(delegate (List<string> x, List<string> y)
                            {

                                return Convert.ToUInt32(x[0].Substring(7)).CompareTo(Convert.ToUInt32(y[0].Substring(7)));
                            });
                        }
                        sbtTag[1] = "";
                        sbt.Tag = sbtTag;
                        leftoutGrid.Children.Add(sbt);
                    }
                    break;
                }
            }
            foreach (StyleButton sbt in leftoutGrid.Children)
            {
                if (!sbt.IsEnabled)
                {
                    sbt.IsEnabled = true;
                    List<string> sbtTag = (List<string>)sbt.Tag;
                    int col = Grid.GetColumn(sbt);
                    int row = Grid.GetRow(sbt);
                    List<string> sbtpos = new List<string>()
                    { sbt.Name,col.ToString(),row.ToString()};
                    PassiveButtonList.Add(sbtpos);
                    PassiveButtonList.Sort(delegate (List<string> x, List<string> y)
                    {
                        return Convert.ToUInt32(x[0].Substring(7)).CompareTo(Convert.ToUInt32(y[0].Substring(7)));
                    });
                    leftoutGrid.Children.Remove(sbt);

                    if (ActiveButtonList.Count >0)
                    {
                        Grid.SetColumn(sbt, Convert.ToInt32(ActiveButtonList[0][1]));
                        Grid.SetRow(sbt, Convert.ToInt32(ActiveButtonList[0][2]));
                        sbt.Name = ActiveButtonList[0][0];
                        ActiveButtonList.RemoveAt(0);
                        ActiveButtonList.Sort(delegate (List<string> x, List<string> y)
                        {
                            return Convert.ToUInt32(x[0].Substring(6)).CompareTo(Convert.ToUInt32(y[0].Substring(6)));
                        });

                    }
                    sbtTag[1] = sbt.Name;
                    sbt.Tag = sbtTag;
                    smallGrid.Children.Add(sbt);
                    break;
                }
            }
        }

        public class StyleButton : Button
        {
            public StyleButton()
            {
                Width = 25;
                Height = 25;
            }
        }

        private void saveToFile_Click(object sender, RoutedEventArgs e)
        {
            foreach (StyleButton bn in smallGrid.Children)
            {
                List<string> ButtonData = (List<string>)bn.Tag;
                ButtonConfigObject buttonobject = new ButtonConfigObject
                {
                    ButtonLabel = ButtonData[0],
                    ButtonID = ButtonData[1],
                    AssignedPicturePath = ButtonData[2],
                    SuperToolTip = ButtonData[3],
                    ToolTip = ButtonData[4],
                    KeyTip = ButtonData[5],
                    AttachedStyle = ButtonData[6]
                };
                BCOList.Add(buttonobject);
            }
            ButtonConfigList buttonConfigList = new ButtonConfigList();
            buttonConfigList.ButtonConfigs = BCOList;
            string buttonToFile = JsonConvert.SerializeObject(buttonConfigList, Formatting.Indented);
            File.WriteAllText(path + @"\ButtonConfig.txt", Extract.EncryptFile(buttonToFile));
            //File.WriteAllText(path + @"\ButtonConfig_notEncrypted.txt", buttonToFile);

            List<string> SelectedTables = new List<string>();
            foreach (CheckBox checks in chkBoxStackPanel.Children)
            {
                
                if (checks.IsChecked.Value)
                {
                    SelectedTables.Add(checks.Content.ToString());
                    int ind = Int32.Parse(_StyleDicList.StyleNameList.FirstOrDefault(x => x.XmlStyleID == checks.Content.ToString()).StyleIndex);
                    _StyleDicList.StyleNameList[ind-1].PreferredStyle = "UserTable";
                }
          
            }
            Extract.RemoveStylesFromJSON(SelectedTables);

            foreach (CheckBox clickedchkBox in grdListStyles.Children.OfType<CheckBox>())
            {
                List<string> ckbTag = (List<string>)clickedchkBox.Tag;
                if (clickedchkBox.IsChecked == true)
                {
                    if (clickedchkBox.Content.ToString().Contains("Numbered"))
                    {
                        int step = 0;
                        foreach (string StyleName in ckbTag)
                        {
                            if (step > 0)
                            {
                                int ind = Int32.Parse(_StyleDicList.StyleNameList.FirstOrDefault(x => x.XmlStyleID == StyleName).StyleIndex);
                                _StyleDicList.StyleNameList[ind - 1].PreferredStyle = "UserNumberedList" + step;
                            }
                            step++;
                        }
                    }
                    if (clickedchkBox.Content.ToString().Contains("Bulleted"))
                    {
                        int step = 0;
                        foreach (string StyleName in ckbTag)
                        {
                            if (step > 0)
                            {
                                int ind = Int32.Parse(_StyleDicList.StyleNameList.FirstOrDefault(x => x.XmlStyleID == StyleName).StyleIndex);
                                _StyleDicList.StyleNameList[ind - 1].PreferredStyle = "UserBulletedList" + step;
                            }
                            step++;
                        }
                    }
                }
            }

            Extract.WriteConfig(path);
            File.WriteAllText(path + @"\StyleListDic.txt", Extract.EncryptFile(JsonConvert.SerializeObject(_StyleDicList, Formatting.Indented)));
            //File.WriteAllText(path + @"\StyleListDic_notEncrypted.txt", JsonConvert.SerializeObject(_StyleDicList, Formatting.Indented));
            resultBox.Text += "Visual Config Creation Successfully Completed ...";
            MessageBox.Show("Visual Config Creation Successfully Completed ...", "Completed", MessageBoxButton.OK);

            this.Close();
        }

       

        private void txb_ConfigName_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            if (!string.IsNullOrEmpty(filenamewpath) && txb_ConfigName.Text != "")
                btnExtract.IsEnabled = true;
            else
               btnExtract.IsEnabled = false;
        }
        private static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
        private void txb_ConfigName_LostFocus(object sender, RoutedEventArgs e)
        {
            string str_ConfigName = txb_ConfigName.Text;
            //txb_ConfigName.Text=CleanFileName(txb_ConfigName.Text);
            // txb_ConfigName.Text = txb_ConfigName.Text.Replace(" ", "");
            // txb_ConfigName.Text = txb_ConfigName.Text.Replace(".", "");
            if (!str_ConfigName.All(Char.IsLetterOrDigit))
            {
                MessageBox.Show("The configuration name specified is invalid! Please use only alphanumeric characters!");

            }

            //if (!string.IsNullOrEmpty(txb_ConfigName.Text) && str_ConfigName!= txb_ConfigName.Text)
              //  MessageBox.Show("The configuration name specified contained some illegal characters that were removed." + Environment.NewLine + "Before proceeding, check the new configuration name !", "Illegal Characters removed !",MessageBoxButton.OK);
        }

        private void TableListFunction(List<string> TableList)
        {
            foreach(var elements in TableList)
            {
                CheckBox check = new CheckBox();
                check.Content = elements;
                check.IsChecked = true;
                chkBoxStackPanel.Children.Add(check);
            }
        }

        private void Image_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Thickness margin = grd_ButtonConfig.Margin;
            margin.Left = -386;
            grd_ButtonConfig.Margin = margin;
        }

        private void Image_MouseLeftButtonDown_back(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Thickness margin = grd_ButtonConfig.Margin;
            margin.Left = 0;
            grd_ButtonConfig.Margin = margin;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }
    }
}
